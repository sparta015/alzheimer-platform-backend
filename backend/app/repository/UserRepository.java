package repository;

import io.ebean.*;
import models.User;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class UserRepository {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext databaseExecutionContext;

    @Inject
    public UserRepository(EbeanConfig ebeanConfig,
                               DatabaseExecutionContext databaseExecutionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.databaseExecutionContext = databaseExecutionContext;
    }

    public CompletionStage<Optional<User>> getUser(String email, String password) {
        return supplyAsync(() -> Optional.ofNullable(ebeanServer.find(User.class).fetch("user")
                .where()
                .eq("email", email)
                .eq("passwd", password)
                .findOne()), databaseExecutionContext);
    }
}
