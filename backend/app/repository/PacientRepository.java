package repository;

import io.ebean.Ebean;
import io.ebean.EbeanServer;
import models.Pacient;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class PacientRepository {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext databaseExecutionContext;

    @Inject
    public PacientRepository(EbeanConfig ebeanConfig,
                                DatabaseExecutionContext databaseExecutionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.databaseExecutionContext = databaseExecutionContext;
    }

    public CompletionStage<Long> insert(Pacient pacient) {
        return supplyAsync(() -> {
            ebeanServer.insert(pacient);
            return pacient.getId();
        }, databaseExecutionContext);
    }

}
