package repository;

import io.ebean.Ebean;
import io.ebean.EbeanServer;
import models.Researcher;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class ResearcherRepository {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext databaseExecutionContext;

    @Inject
    public ResearcherRepository(EbeanConfig ebeanConfig,
                               DatabaseExecutionContext databaseExecutionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.databaseExecutionContext = databaseExecutionContext;
    }

    public CompletionStage<Long> insert(Researcher researcher) {
        return supplyAsync(() -> {
            ebeanServer.insert(researcher);
            return researcher.getId();
        }, databaseExecutionContext);
    }
}
