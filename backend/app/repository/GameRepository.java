package repository;

import io.ebean.Ebean;
import io.ebean.EbeanServer;
import io.ebean.Model;
import io.ebean.Transaction;
import models.Game;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class GameRepository {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext databaseExecutionContext;

    @Inject
    public GameRepository(EbeanConfig ebeanConfig,
                          DatabaseExecutionContext databaseExecutionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.databaseExecutionContext = databaseExecutionContext;
    }

    public CompletionStage<Optional<List<Game>>> getGames() {
        return supplyAsync(() -> Optional.ofNullable(ebeanServer.find(Game.class).findList()), databaseExecutionContext);
    }

    public CompletionStage<Optional<Game>> getGame (Long id) {
        return supplyAsync(() -> Optional.ofNullable(ebeanServer.find(Game.class).setId(id).findOne()), databaseExecutionContext);
    }

    public CompletionStage<Long> create(Game game) {
        return supplyAsync(() -> {
            ebeanServer.insert(game);
            return game.getId();
        }, databaseExecutionContext);
    }

    public CompletionStage<Optional<Long>> update(Long id, Game game) {
        return supplyAsync(() -> {
            Transaction transaction = ebeanServer.beginTransaction();
            Optional<Long> value = Optional.empty();
            try {
                Game gameToUpdate = ebeanServer.find(Game.class).setId(id).findOne();
                gameToUpdate.setName(game.getName());
                gameToUpdate.setDescription(game.getDescription());
                gameToUpdate.setUrl(game.getUrl());

                gameToUpdate.update();
                transaction.commit();
                value = Optional.of(id);
            } finally {
                transaction.end();
            }
            return value;
        }, databaseExecutionContext);
    }

    public CompletionStage<Optional<Long>> delete(Long id) {
        return supplyAsync(() -> {
            try {
                final Optional<Game> optionalGame = Optional.ofNullable(ebeanServer.find(Game.class).setId(id).findOne());
                optionalGame.ifPresent(Model::delete);
                return optionalGame.map(c -> c.id);
            } catch(Exception e) {
                return Optional.empty();
            }
        }, databaseExecutionContext);
    }
}
