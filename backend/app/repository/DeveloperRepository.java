package repository;

import io.ebean.*;
import models.Developer;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class DeveloperRepository {

  private final EbeanServer ebeanServer;
  private final DatabaseExecutionContext databaseExecutionContext;

  @Inject
  public DeveloperRepository(EbeanConfig ebeanConfig,
                             DatabaseExecutionContext databaseExecutionContext) {
    this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
    this.databaseExecutionContext = databaseExecutionContext;
  }

  public CompletionStage<Long> insert(Developer developer) {
    return supplyAsync(() -> {
      ebeanServer.insert(developer);
      return developer.getId();
    }, databaseExecutionContext);
  }
}
