package models;

import javax.persistence.Column;

public class Researcher extends User {

    @Column
    public String organization;

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }
}
