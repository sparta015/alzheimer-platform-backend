package models;

import play.data.validation.Constraints;

import javax.persistence.Column;

public class Game extends BaseModel{

    @Column
    @Constraints.Required
    private String name;

    @Column
    @Constraints.Required
    private String url;

    @Column
    @Constraints.Required
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
