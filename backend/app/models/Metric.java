package models;

import javax.persistence.Column;
import java.time.OffsetDateTime;

public class Metric extends BaseModel{

    @Column
    public OffsetDateTime dateOfPlay;

    @Column
    public int fails;

    @Column
    public String gender;

    @Column
    public int age;

    @Column
    public String country;

    @Column
    public String region;

    public OffsetDateTime getGameTime() {
        return dateOfPlay;
    }

    public void setGameTime(OffsetDateTime dateOfPlay) {
        this.dateOfPlay = dateOfPlay;
    }

    public int getFails() {
        return fails;
    }

    public void setFails(int fails) {
        this.fails = fails;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
