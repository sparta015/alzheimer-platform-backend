package models;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Pacient extends User {

  @Column
  private int age;

  @Column
  private String country;

  @Column
  private String region;

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }
}
