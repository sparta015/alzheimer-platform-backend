package models;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Developer extends User {

  @Column
  private String key;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }
}
