package models;

import javax.persistence.Column;

public class User extends BaseModel {

  private static final long serialVersionUID = 1L;

  @Column
  private String passwd;

  @Column
  private String email;

  public String getPasswd() {
    return passwd;
  }

  public void setPasswd(String passwd) {
    this.passwd = passwd;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
