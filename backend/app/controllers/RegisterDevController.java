package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Developer;
import play.Logger;
import play.libs.concurrent.HttpExecutionContext;
import repository.DeveloperRepository;
import play.mvc.Http.RequestBody;
import play.mvc.Http.Request;
import play.mvc.Result;
import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

import static play.mvc.Controller.request;
import static play.mvc.Results.ok;

public class RegisterDevController {

  private final DeveloperRepository developerRepository;
  private final HttpExecutionContext httpExecutionContext;

  @Inject
  public RegisterDevController(DeveloperRepository developerRepository,
                               HttpExecutionContext httpExecutionContext) {
    this.developerRepository = developerRepository;
    this.httpExecutionContext = httpExecutionContext;
  }

  public CompletionStage<Result> create() throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    JsonNode body = request().body().asJson();
    Developer newDeveloper = mapper.treeToValue(body, Developer.class);
    System.out.println(newDeveloper.getEmail());
    return developerRepository.insert(newDeveloper).thenApplyAsync(id -> {
        return ok("El identificador es :" + id);
    }, httpExecutionContext.current());
  }

  public Result hello() {
    return ok("Esto tira nene");
  }

}
