package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Researcher;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Result;
import repository.ResearcherRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

import static play.mvc.Controller.request;
import static play.mvc.Results.ok;

public class RegisterResearcherController {

  private final ResearcherRepository researcherRepository;
  private final HttpExecutionContext httpExecutionContext;

  @Inject
  public RegisterResearcherController(ResearcherRepository researcherRepository,
                                      HttpExecutionContext httpExecutionContext) {
    this.researcherRepository = researcherRepository;
    this.httpExecutionContext = httpExecutionContext;
  }

  public CompletionStage<Result> create() throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    JsonNode body = request().body().asJson();
    Researcher newResearcher = mapper.treeToValue(body, Researcher.class);
    System.out.println(newResearcher.getEmail());
    return researcherRepository.insert(newResearcher).thenApplyAsync(id -> {
        return ok("El identificador es :" + id);
    }, httpExecutionContext.current());
  }

  public Result hello() {
    return ok("Esto tira nene");
  }

}
