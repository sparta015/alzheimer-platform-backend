package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Game;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import repository.GameRepository;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.mvc.Controller.request;
import static play.mvc.Results.badRequest;
import static play.mvc.Results.ok;

public class GameController {

    private final GameRepository gameRepository;
    private final HttpExecutionContext httpExecutionContext;

    @Inject
    public GameController(GameRepository gameRepository,
                          HttpExecutionContext httpExecutionContext){
        this.gameRepository = gameRepository;
        this.httpExecutionContext = httpExecutionContext;
    }

    public CompletionStage<Result> list() {

        return gameRepository.getGames().thenApplyAsync(games ->
                ok(Json.toJson(games)), httpExecutionContext.current());
    }

    public CompletionStage<Result> getGame(Long id) {
        return gameRepository.getGame(id).thenApplyAsync(game ->
                ok(Json.toJson(game)), httpExecutionContext.current());
    }

    public CompletionStage<Result> deleteGame(Long id) {
        return gameRepository.delete(id).thenApplyAsync(idDeleteGame ->
                ok("Ejercicio borrado: "+ idDeleteGame), httpExecutionContext.current());

    }

    public CompletionStage<Result> createGame() {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode body = request().body().asJson();
        Game newGame = null;
        try {
            newGame = mapper.treeToValue(body, Game.class);
        } catch (JsonProcessingException e) {
            return CompletableFuture.completedFuture(badRequest("El contenido del mensaje no es correcto"));
        }
        return gameRepository.create(newGame).thenApplyAsync(id ->
            ok("El identificador del nuevo ejecicio es: " + id)
        , httpExecutionContext.current());
    }

    public CompletionStage<Result> updateGame(Long id) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode body = request().body().asJson();
        Game updateGame = null;
        try {
            updateGame = mapper.treeToValue(body, Game.class);
        } catch (JsonProcessingException e) {
            return CompletableFuture.completedFuture(badRequest("El contenido del mensaje no es correcto"));
        }
        return gameRepository.update(id, updateGame).thenApplyAsync(idGameUpdated ->
            ok("El identificar del ejercicio actualizado es:" + idGameUpdated), httpExecutionContext.current());
    }
}
