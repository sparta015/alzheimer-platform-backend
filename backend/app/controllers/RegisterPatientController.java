package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Pacient;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Result;
import repository.PacientRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

import static play.mvc.Controller.request;
import static play.mvc.Results.ok;

public class RegisterPatientController {

  private final PacientRepository pacientRepository;
  private final HttpExecutionContext httpExecutionContext;

  @Inject
  public RegisterPatientController(PacientRepository pacientRepository,
                                   HttpExecutionContext httpExecutionContext) {
    this.pacientRepository = pacientRepository;
    this.httpExecutionContext = httpExecutionContext;
  }

  public CompletionStage<Result> create() throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    JsonNode body = request().body().asJson();
    Pacient newPacient = mapper.treeToValue(body, Pacient.class);
    System.out.println(newPacient.getEmail());
    return pacientRepository.insert(newPacient).thenApplyAsync(id -> {
        return ok("El identificador de paciente es :" + id);
    }, httpExecutionContext.current());
  }

  public Result hello() {
    return ok("Esto tira nene");
  }

}
