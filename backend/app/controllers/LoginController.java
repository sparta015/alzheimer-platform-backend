package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.User;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Result;
import repository.UserRepository;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static play.mvc.Controller.request;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;

public class LoginController {

    private final UserRepository userRepository;
    private final HttpExecutionContext httpExecutionContext;

    @Inject
    public LoginController(UserRepository userRepository,
                           HttpExecutionContext httpExecutionContext) {
        this.userRepository = userRepository;
        this.httpExecutionContext = httpExecutionContext;
    }

    public CompletionStage<Result> login() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode body = request().body().asJson();
        User userToLogin = mapper.treeToValue(body, User.class);
        System.out.println(userToLogin.getEmail());
        return userRepository.getUser(userToLogin.getEmail(), userToLogin.getPasswd())
                .thenApplyAsync(user -> response(user), httpExecutionContext.current());
    }

    private Result response(Optional<User> user) {
        if (user.isPresent()) return ok();
        return notFound();
    }
}