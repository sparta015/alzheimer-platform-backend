import io.ebean.PagedList;
import models.Game;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.WithApplication;
import repository.GameRepository;

import java.util.Date;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

public class ModelTest extends WithApplication {

    Game gameWithNull;
    Long gameId;
    String gameUrl;
    String gameName;
    String gameDescription;

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    @Before
    public void setUp(){
        gameWithNull = new Game();
        gameUrl = null;
        gameName = "ejercicioRestas";
        gameDescription = "greatGame";
        gameWithNull.setUrl(gameUrl);
        gameWithNull.setName(gameName);
        gameWithNull.setDescription(gameDescription);

    }

    private String formatted(Date date) {
        return new java.text.SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    @Test
    public void findById() {

        final GameRepository computerRepository = app.injector().instanceOf(GameRepository.class);
        final CompletionStage<Optional<Game>> gameFromPersistence = computerRepository.getGame(21L);

        await().atMost(1, SECONDS).until(() ->
            assertThat(gameFromPersistence.toCompletableFuture()).isCompletedWithValueMatching(gameOptional -> {
                final Game game = gameOptional.get();
                return (game.getName().equals("EjercicioSuma"));
            })
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertWithNull() throws Throwable {
        final GameRepository computerRepository = app.injector().instanceOf(GameRepository.class);
        final CompletionStage<Long> gameIdForSuccess = computerRepository.create(gameWithNull);
    }
    
}
