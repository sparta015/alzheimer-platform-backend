# --- First database schema

# --- !Ups

create table usuario (
  id                        bigint not null PRIMARY KEY,
  passwd                    varchar(8),
  email                     varchar(20)
);

create table developer (
  id                        bigint not null,
  name                      varchar(255),
  passwd                    varchar(8),
  email                     varchar(255),
  secretKey                 varchar(8),
  constraint pk_developer primary key (id))
;

create table patient (
  id                        bigint not null,
  name                      varchar(255),
  passwd                    varchar(8),
  age                       int,
  country                   varchar(255),
  region                    varchar(255),
  constraint pk_patient primary key (id))
;

create table metrics (
  id                        bigint not null,
  dateOfPlay                TIMESTAMP,
  fails                     int,
  gender                    varchar(1),
  age                       int,
  country                   varchar(255),
  region                    varchar(255),
  constraint pk_metrics primary key (id))
;

create sequence developer_seq start with 1000;

create sequence pacient_seq start with 1000;


# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;


drop table if exists developer;

drop table if exists pacient;

SET REFERENTIAL_INTEGRITY TRUE;


drop sequence if exists developer_seq;

drop sequence if exists pacient_seq;

