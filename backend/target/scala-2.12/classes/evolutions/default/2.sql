# --- Sample dataset

# --- !Ups

insert into developer (id,name,passwd,email,secretKey) values (  1,'guille','test','a@e.com','aeiouzxc');


create table metrics (
  id                        bigint not null,
  dateOfPlay                TIMESTAMP,
  fails                     int,
  gender                    varchar(1),
  age                       int,
  country                   varchar(255),
  region                    varchar(255),
  constraint pk_metrics primary key (id))
;

insert into metrics (id,dateOfPlay,fails,gender,age,country,region) values (  1,'1999-01-08 04:05:06',1,'H',56,'Spain','CyL');

# --- !Downs

delete from developer;
