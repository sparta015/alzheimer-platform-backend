// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/gpd/Documents/tfg/menu2/registro/conf/routes
// @DATE:Wed Jul 11 10:37:47 CEST 2018

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseGameController GameController = new controllers.ReverseGameController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseRegisterDevController RegisterDevController = new controllers.ReverseRegisterDevController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseLoginController LoginController = new controllers.ReverseLoginController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseRegisterPatientController RegisterPatientController = new controllers.ReverseRegisterPatientController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseRegisterResearcherController RegisterResearcherController = new controllers.ReverseRegisterResearcherController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseGameController GameController = new controllers.javascript.ReverseGameController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseRegisterDevController RegisterDevController = new controllers.javascript.ReverseRegisterDevController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseLoginController LoginController = new controllers.javascript.ReverseLoginController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseRegisterPatientController RegisterPatientController = new controllers.javascript.ReverseRegisterPatientController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseRegisterResearcherController RegisterResearcherController = new controllers.javascript.ReverseRegisterResearcherController(RoutesPrefix.byNamePrefix());
  }

}
