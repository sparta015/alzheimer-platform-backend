// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/gpd/Documents/tfg/menu2/registro/conf/routes
// @DATE:Wed Jul 11 10:37:47 CEST 2018

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:7
  RegisterDevController_1: controllers.RegisterDevController,
  // @LINE:8
  RegisterPatientController_3: controllers.RegisterPatientController,
  // @LINE:9
  RegisterResearcherController_2: controllers.RegisterResearcherController,
  // @LINE:13
  GameController_0: controllers.GameController,
  // @LINE:20
  LoginController_5: controllers.LoginController,
  // @LINE:25
  Assets_4: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:7
    RegisterDevController_1: controllers.RegisterDevController,
    // @LINE:8
    RegisterPatientController_3: controllers.RegisterPatientController,
    // @LINE:9
    RegisterResearcherController_2: controllers.RegisterResearcherController,
    // @LINE:13
    GameController_0: controllers.GameController,
    // @LINE:20
    LoginController_5: controllers.LoginController,
    // @LINE:25
    Assets_4: controllers.Assets
  ) = this(errorHandler, RegisterDevController_1, RegisterPatientController_3, RegisterResearcherController_2, GameController_0, LoginController_5, Assets_4, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, RegisterDevController_1, RegisterPatientController_3, RegisterResearcherController_2, GameController_0, LoginController_5, Assets_4, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """register/developer""", """controllers.RegisterDevController.create()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """register/patient""", """controllers.RegisterPatientController.create()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """register/researcher""", """controllers.RegisterResearcherController.create()"""),
    ("""GET""", this.prefix, """controllers.RegisterDevController.hello()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """games""", """controllers.GameController.list()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """games/""" + "$" + """id<[^/]+>""", """controllers.GameController.getGame(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """games/""" + "$" + """id<[^/]+>""", """controllers.GameController.deleteGame(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """games""", """controllers.GameController.createGame()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """games/""" + "$" + """id<[^/]+>""", """controllers.GameController.updateGame(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login""", """controllers.LoginController.login()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.at(path:String = "/public", file:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:7
  private[this] lazy val controllers_RegisterDevController_create0_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("register/developer")))
  )
  private[this] lazy val controllers_RegisterDevController_create0_invoker = createInvoker(
    RegisterDevController_1.create(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RegisterDevController",
      "create",
      Nil,
      "POST",
      this.prefix + """register/developer""",
      """""",
      Seq()
    )
  )

  // @LINE:8
  private[this] lazy val controllers_RegisterPatientController_create1_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("register/patient")))
  )
  private[this] lazy val controllers_RegisterPatientController_create1_invoker = createInvoker(
    RegisterPatientController_3.create(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RegisterPatientController",
      "create",
      Nil,
      "POST",
      this.prefix + """register/patient""",
      """""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_RegisterResearcherController_create2_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("register/researcher")))
  )
  private[this] lazy val controllers_RegisterResearcherController_create2_invoker = createInvoker(
    RegisterResearcherController_2.create(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RegisterResearcherController",
      "create",
      Nil,
      "POST",
      this.prefix + """register/researcher""",
      """""",
      Seq()
    )
  )

  // @LINE:10
  private[this] lazy val controllers_RegisterDevController_hello3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_RegisterDevController_hello3_invoker = createInvoker(
    RegisterDevController_1.hello(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RegisterDevController",
      "hello",
      Nil,
      "GET",
      this.prefix + """""",
      """""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_GameController_list4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("games")))
  )
  private[this] lazy val controllers_GameController_list4_invoker = createInvoker(
    GameController_0.list(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GameController",
      "list",
      Nil,
      "GET",
      this.prefix + """games""",
      """ Path for menu""",
      Seq()
    )
  )

  // @LINE:14
  private[this] lazy val controllers_GameController_getGame5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("games/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GameController_getGame5_invoker = createInvoker(
    GameController_0.getGame(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GameController",
      "getGame",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """games/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:15
  private[this] lazy val controllers_GameController_deleteGame6_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("games/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GameController_deleteGame6_invoker = createInvoker(
    GameController_0.deleteGame(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GameController",
      "deleteGame",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """games/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_GameController_createGame7_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("games")))
  )
  private[this] lazy val controllers_GameController_createGame7_invoker = createInvoker(
    GameController_0.createGame(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GameController",
      "createGame",
      Nil,
      "POST",
      this.prefix + """games""",
      """""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_GameController_updateGame8_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("games/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GameController_updateGame8_invoker = createInvoker(
    GameController_0.updateGame(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GameController",
      "updateGame",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """games/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:20
  private[this] lazy val controllers_LoginController_login9_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login")))
  )
  private[this] lazy val controllers_LoginController_login9_invoker = createInvoker(
    LoginController_5.login(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LoginController",
      "login",
      Nil,
      "POST",
      this.prefix + """login""",
      """ Path for login""",
      Seq()
    )
  )

  // @LINE:25
  private[this] lazy val controllers_Assets_at10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_at10_invoker = createInvoker(
    Assets_4.at(fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "at",
      Seq(classOf[String], classOf[String]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:7
    case controllers_RegisterDevController_create0_route(params@_) =>
      call { 
        controllers_RegisterDevController_create0_invoker.call(RegisterDevController_1.create())
      }
  
    // @LINE:8
    case controllers_RegisterPatientController_create1_route(params@_) =>
      call { 
        controllers_RegisterPatientController_create1_invoker.call(RegisterPatientController_3.create())
      }
  
    // @LINE:9
    case controllers_RegisterResearcherController_create2_route(params@_) =>
      call { 
        controllers_RegisterResearcherController_create2_invoker.call(RegisterResearcherController_2.create())
      }
  
    // @LINE:10
    case controllers_RegisterDevController_hello3_route(params@_) =>
      call { 
        controllers_RegisterDevController_hello3_invoker.call(RegisterDevController_1.hello())
      }
  
    // @LINE:13
    case controllers_GameController_list4_route(params@_) =>
      call { 
        controllers_GameController_list4_invoker.call(GameController_0.list())
      }
  
    // @LINE:14
    case controllers_GameController_getGame5_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_GameController_getGame5_invoker.call(GameController_0.getGame(id))
      }
  
    // @LINE:15
    case controllers_GameController_deleteGame6_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_GameController_deleteGame6_invoker.call(GameController_0.deleteGame(id))
      }
  
    // @LINE:16
    case controllers_GameController_createGame7_route(params@_) =>
      call { 
        controllers_GameController_createGame7_invoker.call(GameController_0.createGame())
      }
  
    // @LINE:17
    case controllers_GameController_updateGame8_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_GameController_updateGame8_invoker.call(GameController_0.updateGame(id))
      }
  
    // @LINE:20
    case controllers_LoginController_login9_route(params@_) =>
      call { 
        controllers_LoginController_login9_invoker.call(LoginController_5.login())
      }
  
    // @LINE:25
    case controllers_Assets_at10_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at10_invoker.call(Assets_4.at(path, file))
      }
  }
}
