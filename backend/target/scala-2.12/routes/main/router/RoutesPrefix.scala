// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/gpd/Documents/tfg/menu2/registro/conf/routes
// @DATE:Wed Jul 11 10:37:47 CEST 2018


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
